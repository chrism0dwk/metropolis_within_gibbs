"""Test the random walk metropolis kernel"""

from functools import partial
from typing import NamedTuple
import numpy as np
import jax
import tensorflow_probability.substrates.jax as tfp
from mwg.mcmc import Step, rwmh
from mwg.mcmc.random_walk_metropolis import RwmhInfo


class State(NamedTuple):
    foo: float
    bar: float
    baz: float


@tfp.distributions.JointDistributionCoroutine
def simple_model():
    yield tfp.distributions.Normal(loc=0.0, scale=1.0, name="foo")
    yield tfp.distributions.Normal(loc=1.0, scale=1.0, name="bar")
    yield tfp.distributions.Normal(loc=2.0, scale=1.0, name="baz")


def test_rwmh_1kernel():
    seed = jax.random.PRNGKey(42)

    initial_position = simple_model.sample(seed=seed)

    kernel = rwmh(scale=0.3)

    state = kernel.init(simple_model.log_prob, initial_position)
    new_state, results = kernel.step(simple_model.log_prob, state, seed)

    assert jax.tree_map(lambda x, y: None, new_state, state)

    expected_results = (RwmhInfo(is_accepted=True),)
    assert jax.tree_map(lambda x, y: x == y, results, expected_results)


def test_rwmh_2kernel():
    seed = jax.random.PRNGKey(42)

    initial_position = simple_model.sample(seed=seed)

    kernel = Step(rwmh(scale=0.3), ["foo"]) >> Step(rwmh(scale=0.3), ["bar", "baz"])

    state = kernel.init(simple_model.log_prob, initial_position)
    new_state, results = kernel.step(simple_model.log_prob, state, seed)

    assert jax.tree_map(lambda x, y: None, new_state, state)

    expected_results = (RwmhInfo(is_accepted=True), RwmhInfo(is_accepted=True))
    assert jax.tree_map(lambda x, y: x == y, results, expected_results)


def test_rwmh_3kernel():
    seed = jax.random.PRNGKey(42)

    initial_position = simple_model.sample(seed=seed)

    kernel = (
        Step(rwmh(scale=0.3), ["foo"])
        >> Step(rwmh(scale=0.1), ["bar"])
        >> Step(rwmh(scale=0.2), ["baz"])
    )

    state = kernel.init(simple_model.log_prob, initial_position)

    new_state, results = kernel.step(simple_model.log_prob, state, seed)

    assert jax.tree_map(lambda x, y: None, new_state, state)

    expected_results = (
        RwmhInfo(is_accepted=True),
        RwmhInfo(is_accepted=True),
        RwmhInfo(is_accepted=True),
    )
    assert jax.tree_map(lambda x, y: x == y, results, expected_results)


def test_rwmh_1kernel_mcmc():
    seed = jax.random.PRNGKey(42)

    initial_position = simple_model.sample(seed=seed)

    kernel = rwmh(scale=1.8)
    kernel_step_fn = partial(kernel.step, simple_model.log_prob)

    def one_step(state, rng_key):
        new_state, results = kernel_step_fn(state, rng_key)
        return new_state, (new_state[0].position, results)

    num_samples = 100000
    keys = jax.random.split(seed, num_samples)
    initial_state = kernel.init(simple_model.log_prob, initial_position)

    _, (posterior, results) = jax.lax.scan(one_step, initial_state, keys)

    # Test results
    np.testing.assert_approx_equal(results[0].is_accepted.mean(), 0.23, significant=1)
    np.testing.assert_allclose(
        jax.tree_map(lambda x: x.mean(), posterior),
        [0.0, 1.0, 2.0],
        rtol=0.01,
        atol=0.05,
    )
    np.testing.assert_allclose(
        jax.tree_map(lambda x: x.var(), posterior),
        [1.0, 1.0, 1.0],
        rtol=0.01,
        atol=0.05,
    )


def test_rwmh_2kernel_mcmc():
    seed = jax.random.PRNGKey(42)

    initial_position = simple_model.sample(seed=seed)

    kernel = Step(rwmh(scale=2.3), ["foo"]) >> Step(rwmh(scale=1.8), ["bar", "baz"])

    kernel_step_fn = partial(kernel.step, simple_model.log_prob)

    def one_step(state, rng_key):
        new_state, results = kernel_step_fn(state, rng_key)
        return new_state, (new_state[0].position, results)

    num_samples = 100000
    keys = jax.random.split(seed, num_samples)
    initial_state = kernel.init(simple_model.log_prob, initial_position)

    _, (posterior, results) = jax.lax.scan(one_step, initial_state, keys)

    # Test results
    np.testing.assert_allclose(
        jax.tree_util.tree_flatten(jax.tree_map(lambda x: x.mean(), results))[0],
        [0.45, 0.33],
        atol=0.01,
        rtol=0.05,
    )
    np.testing.assert_allclose(
        jax.tree_map(lambda x: x.mean(), posterior),
        [0.0, 1.0, 2.0],
        rtol=0.01,
        atol=0.05,
    )
    np.testing.assert_allclose(
        jax.tree_map(lambda x: x.var(), posterior),
        [1.0, 1.0, 1.0],
        rtol=0.01,
        atol=0.05,
    )


def test_rwmh_3kernel_mcmc():
    seed = jax.random.PRNGKey(42)

    initial_position = simple_model.sample(seed=seed)

    kernel = (
        Step(rwmh(scale=2.3), ["foo"])
        >> Step(rwmh(scale=2.3), ["bar"])
        >> Step(rwmh(scale=2.3), ["baz"])
    )

    kernel_step_fn = partial(kernel.step, simple_model.log_prob)

    def one_step(state, rng_key):
        new_state, results = kernel_step_fn(state, rng_key)
        return new_state, (new_state[0].position, results)

    num_samples = 100000
    keys = jax.random.split(seed, num_samples)
    initial_state = kernel.init(simple_model.log_prob, initial_position)

    _, (posterior, results) = jax.lax.scan(one_step, initial_state, keys)

    # Test results
    np.testing.assert_allclose(
        jax.tree_util.tree_flatten(jax.tree_map(lambda x: x.mean(), results))[0],
        [0.45, 0.45, 0.45],
        atol=0.01,
        rtol=0.05,
    )
    np.testing.assert_allclose(
        jax.tree_map(lambda x: x.mean(), posterior),
        [0.0, 1.0, 2.0],
        rtol=0.01,
        atol=0.05,
    )
    np.testing.assert_allclose(
        jax.tree_map(lambda x: x.var(), posterior),
        [1.0, 1.0, 1.0],
        rtol=0.01,
        atol=0.05,
    )
